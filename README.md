# Dockerized mlmmj mailing list manager

## Setup

### Postfix

A minimum setup needs the follow env variables set by providing
them in an `.env` file.

- `POSTFIX_MYHOSTNAME=`
- `POSTFIX_RELAY_DOMAINS=`

Postfix `myhostname` is the hostname used by postfix.
Postfix `relay_domain` are the domains used by your mailing list addresses.

### mlmmj

mlmmj stores its mailing lists in `/var/spool/mlmmj`. To add a new list issue the following command

    /usr/bin/mlmmj-make-ml -L $name_of_list -s /var/spool/mlmmj -c mlmmj

and answer the questions.

Make sure the domain of the mailing list address is added to `$POSTFIX_RELAY_DOMAINS`

Finally add the address to the transport file in /etc/postfix directory

    list-name@domain.tld         mlmmj:list-name

and run:

    postmap /etc/postfix/transport

## Add users

    /usr/bin/mlmmj-sub -L /path/to/list -a john@doe.org

For more advanced management read the documentation.
