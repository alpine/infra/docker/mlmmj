FROM alpine

RUN apk --no-cache add postfix mlmmj

COPY scripts /usr/local/bin

EXPOSE 25

ENTRYPOINT [ "entrypoint.sh" ]

CMD [ "start-fg" ]
