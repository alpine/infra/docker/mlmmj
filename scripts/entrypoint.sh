#!/bin/sh

postconf -e smtp_tls_security_level=may
postconf -e recipient_delimiter=+
postconf -e maillog_file=/dev/stdout
postconf -e mlmmj_destination_recipient_limit=1
postconf -M mlmmj/unix="mlmmj  unix  -  n  n  -  -  pipe flags=ORhu user=mlmmj \
	argv=/usr/bin/mlmmj-receive -F -L /var/spool/mlmmj/\$nexthop"

newaliases

if [ "$POSTFIX_MYHOSTNAME" ]; then
	postconf -e myhostname="$POSTFIX_MYHOSTNAME"
fi

if [ "$POSTFIX_RELAY_DOMAINS" ]; then
	postconf -e relay_domains="$POSTFIX_RELAY_DOMAINS"
fi

if [ -f "/etc/postfix/transport" ]; then
	postconf -e transport_maps=hash:/etc/postfix/transport
	postmap /etc/postfix/transport
fi

exec postfix $@
